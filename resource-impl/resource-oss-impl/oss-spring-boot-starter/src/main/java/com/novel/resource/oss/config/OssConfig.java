package com.novel.resource.oss.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * OSS 配置
 *
 * @author novel
 * @date 2019/6/4
 */
@ConfigurationProperties(prefix = OssConfig.OSS_PREFIX)
public class OssConfig {
    public static final String OSS_PREFIX = "oss";
    /**
     * oss容器地址
     */
    private String ossEndpoint;
    /**
     * 容器id
     */
    private String ossAccessKeyId;
    /**
     * 容器秘钥
     */
    private String ossAccessKeySecret;
    /**
     * 容器的名称
     */
    private String bucketName;

    public String getOssEndpoint() {
        return ossEndpoint;
    }

    public void setOssEndpoint(String ossEndpoint) {
        this.ossEndpoint = ossEndpoint;
    }

    public String getOssAccessKeyId() {
        return ossAccessKeyId;
    }

    public void setOssAccessKeyId(String ossAccessKeyId) {
        this.ossAccessKeyId = ossAccessKeyId;
    }

    public String getOssAccessKeySecret() {
        return ossAccessKeySecret;
    }

    public void setOssAccessKeySecret(String ossAccessKeySecret) {
        this.ossAccessKeySecret = ossAccessKeySecret;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }
}
