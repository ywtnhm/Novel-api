package com.novel.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.novel.common.constants.ScheduleConstants;
import com.novel.common.utils.StringUtils;
import com.novel.framework.base.BaseModel;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import com.novel.util.CronUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 定时任务调度表 sys_job
 *
 * @author novel
 * @date 2020/3/2
 */
public class SysJob extends BaseModel {
    private static final long serialVersionUID = 1L;

    /**
     * 任务名称
     */
    @Excel(name = "任务名称",width = 20)
    @NotBlank(message = "任务名称不能为空", groups = {AddGroup.class, EditGroup.class})
    @Size(min = 0, max = 64, message = "任务名称不能超过64个字符", groups = {AddGroup.class, EditGroup.class})
    private String jobName;

    /**
     * 任务组名
     */
    @Excel(name = "任务组名", orderNum = "1", replace = {"默认_DEFAULT", "系统_SYSTEM"})
    private String jobGroup;

    /**
     * 调用目标字符串
     */
    @Excel(name = "调用目标字符串", width = 35,orderNum = "2")
    @NotBlank(message = "调用目标字符串不能为空", groups = {AddGroup.class, EditGroup.class})
    @Size(min = 0, max = 1000, message = "调用目标字符串长度不能超过500个字符", groups = {AddGroup.class, EditGroup.class})
    private String invokeTarget;

    /**
     * cron执行表达式
     */
    @Excel(name = "cron执行表达式",width = 20,orderNum = "3")
    @NotBlank(message = "Cron执行表达式不能为空", groups = {AddGroup.class, EditGroup.class})
    @Size(min = 0, max = 255, message = "Cron执行表达式不能超过255个字符", groups = {AddGroup.class, EditGroup.class})
    private String cronExpression;

    /**
     * cron计划策略
     */
    @Excel(name = "cron计划策略", orderNum = "3", replace = {"立即执行_1", "执行一次_2", "放弃执行_3"})
    private String misfirePolicy = ScheduleConstants.MISFIRE_DEFAULT;

    /**
     * 是否并发执行（0允许 1禁止）
     */
    @Excel(name = "并发执行", orderNum = "4", replace = {"允许_0", "禁止_1"})
    private String concurrent;

    /**
     * 任务状态（0正常 1暂停）
     */
    @Excel(name = "任务状态", orderNum = "4", replace = {"正常_0", "暂停_1"})
    private String status;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }


    public String getInvokeTarget() {
        return invokeTarget;
    }

    public void setInvokeTarget(String invokeTarget) {
        this.invokeTarget = invokeTarget;
    }


    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public Date getNextValidTime() {
        if (StringUtils.isNotEmpty(cronExpression)) {
            return CronUtils.getNextExecution(cronExpression);
        }
        return null;
    }

    public String getMisfirePolicy() {
        return misfirePolicy;
    }

    public void setMisfirePolicy(String misfirePolicy) {
        this.misfirePolicy = misfirePolicy;
    }

    public String getConcurrent() {
        return concurrent;
    }

    public void setConcurrent(String concurrent) {
        this.concurrent = concurrent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("jobName", getJobName())
                .append("jobGroup", getJobGroup())
                .append("cronExpression", getCronExpression())
                .append("nextValidTime", getNextValidTime())
                .append("misfirePolicy", getMisfirePolicy())
                .append("concurrent", getConcurrent())
                .append("status", getStatus())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}