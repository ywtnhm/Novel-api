package com.novel.kaptcha.exception;

/**
 * 验证码不正确异常
 *
 * @author novel
 * @date 2019/9/28
 */
public class KaptchaIncorrectException extends KaptchaException {
    public KaptchaIncorrectException(String message) {
        super(message);
    }

    public KaptchaIncorrectException() {
    }
}
